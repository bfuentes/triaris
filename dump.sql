-- MySQL dump 10.13  Distrib 5.5.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: triaris
-- ------------------------------------------------------
-- Server version	5.5.33-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dm_area`
--

DROP TABLE IF EXISTS `dm_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_layout_id` bigint(20) DEFAULT NULL,
  `dm_page_view_id` bigint(20) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_layout_id_idx` (`dm_layout_id`),
  KEY `dm_page_view_id_idx` (`dm_page_view_id`),
  CONSTRAINT `dm_area_dm_layout_id_dm_layout_id` FOREIGN KEY (`dm_layout_id`) REFERENCES `dm_layout` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_area_dm_page_view_id_dm_page_view_id` FOREIGN KEY (`dm_page_view_id`) REFERENCES `dm_page_view` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_area`
--

LOCK TABLES `dm_area` WRITE;
/*!40000 ALTER TABLE `dm_area` DISABLE KEYS */;
INSERT INTO `dm_area` VALUES (1,NULL,2,'1'),(2,NULL,3,'1'),(3,1,NULL,'top'),(4,1,NULL,'left'),(5,1,NULL,'right'),(6,NULL,1,'content'),(7,1,NULL,'bottom'),(8,NULL,3,'content'),(9,NULL,2,'content');
/*!40000 ALTER TABLE `dm_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_auto_seo`
--

DROP TABLE IF EXISTS `dm_auto_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_auto_seo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dmAutoSeoModuleAction_idx` (`module`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_auto_seo`
--

LOCK TABLES `dm_auto_seo` WRITE;
/*!40000 ALTER TABLE `dm_auto_seo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_auto_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_auto_seo_translation`
--

DROP TABLE IF EXISTS `dm_auto_seo_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_auto_seo_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strip_words` text COLLATE utf8_unicode_ci,
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_auto_seo_translation_id_dm_auto_seo_id` FOREIGN KEY (`id`) REFERENCES `dm_auto_seo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_auto_seo_translation`
--

LOCK TABLES `dm_auto_seo_translation` WRITE;
/*!40000 ALTER TABLE `dm_auto_seo_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_auto_seo_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_behavior`
--

DROP TABLE IF EXISTS `dm_behavior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_behavior` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_behavior_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dm_behavior_attached_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dm_behavior_attached_to_selector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dm_page_id` bigint(20) DEFAULT NULL,
  `dm_area_id` bigint(20) DEFAULT NULL,
  `dm_zone_id` bigint(20) DEFAULT NULL,
  `dm_widget_id` bigint(20) DEFAULT NULL,
  `position` bigint(20) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_page_id_idx` (`dm_page_id`),
  KEY `dm_area_id_idx` (`dm_area_id`),
  KEY `dm_zone_id_idx` (`dm_zone_id`),
  KEY `dm_widget_id_idx` (`dm_widget_id`),
  CONSTRAINT `dm_behavior_dm_area_id_dm_area_id` FOREIGN KEY (`dm_area_id`) REFERENCES `dm_area` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_behavior_dm_page_id_dm_page_id` FOREIGN KEY (`dm_page_id`) REFERENCES `dm_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_behavior_dm_widget_id_dm_widget_id` FOREIGN KEY (`dm_widget_id`) REFERENCES `dm_widget` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_behavior_dm_zone_id_dm_zone_id` FOREIGN KEY (`dm_zone_id`) REFERENCES `dm_zone` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_behavior`
--

LOCK TABLES `dm_behavior` WRITE;
/*!40000 ALTER TABLE `dm_behavior` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_behavior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_behavior_translation`
--

DROP TABLE IF EXISTS `dm_behavior_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_behavior_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `dm_behavior_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dm_behavior_value` text COLLATE utf8_unicode_ci,
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_behavior_translation_id_dm_behavior_id` FOREIGN KEY (`id`) REFERENCES `dm_behavior` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_behavior_translation`
--

LOCK TABLES `dm_behavior_translation` WRITE;
/*!40000 ALTER TABLE `dm_behavior_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_behavior_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_bit_ly_url`
--

DROP TABLE IF EXISTS `dm_bit_ly_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_bit_ly_url` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `short` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `expanded` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `short` (`short`),
  KEY `expanded_index_idx` (`expanded`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_bit_ly_url`
--

LOCK TABLES `dm_bit_ly_url` WRITE;
/*!40000 ALTER TABLE `dm_bit_ly_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_bit_ly_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_catalogue`
--

DROP TABLE IF EXISTS `dm_catalogue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_catalogue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_lang` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `target_lang` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_catalogue`
--

LOCK TABLES `dm_catalogue` WRITE;
/*!40000 ALTER TABLE `dm_catalogue` DISABLE KEYS */;
INSERT INTO `dm_catalogue` VALUES (1,'messages.es','en','es'),(2,'dm.es','en','es');
/*!40000 ALTER TABLE `dm_catalogue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_error`
--

DROP TABLE IF EXISTS `dm_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_error` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `php_class` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `module` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `env` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_error`
--

LOCK TABLES `dm_error` WRITE;
/*!40000 ALTER TABLE `dm_error` DISABLE KEYS */;
INSERT INTO `dm_error` VALUES (1,'sfError404Exception','no current DmPage','no current DmPage\n#0 /home/ngc598/www/triaris/dm/dmFrontPlugin/modules/dmPage/actions/actions.class.php(32): sfAction->forward404Unless(NULL, \'no current DmPa...\')\n#1 /home/ngc598/www/triaris/dm/symfony/lib/action/sfActions.class.php(60): dmPageActions->executeEdit(Object(dmWebRequest))\n#2 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(92): sfActions->execute(Object(dmWebRequest))\n#3 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(78): sfExecutionFilter->executeAction(Object(dmPageActions))\n#4 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(42): sfExecutionFilter->handleAction(Object(sfFilterChain), Object(dmPageActions))\n#5 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfExecutionFilter->execute(Object(sfFilterChain))\n#6 /home/ngc598/www/triaris/dm/dmFrontPlugin/lib/filter/dmFrontInitFilter.php(34): sfFilterChain->execute()\n#7 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): dmFrontInitFilter->execute(Object(sfFilterChain))\n#8 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfBasicSecurityFilter.class.php(72): sfFilterChain->execute()\n#9 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfBasicSecurityFilter->execute(Object(sfFilterChain))\n#10 /home/ngc598/www/triaris/dm/dmCorePlugin/plugins/dmUserPlugin/lib/dmRememberMeFilter.class.php(56): sfFilterChain->execute()\n#11 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): dmRememberMeFilter->execute(Object(sfFilterChain))\n#12 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfRenderingFilter.class.php(33): sfFilterChain->execute()\n#13 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfRenderingFilter->execute(Object(sfFilterChain))\n#14 /home/ngc598/www/triaris/dm/symfony/lib/controller/sfController.class.php(238): sfFilterChain->execute()\n#15 /home/ngc598/www/triaris/dm/dmCorePlugin/lib/controller/dmFrontWebController.php(38): sfController->forward(\'dmPage\', \'edit\')\n#16 /home/ngc598/www/triaris/dm/dmCorePlugin/lib/context/dmContext.php(290): dmFrontWebController->dispatch()\n#17 /home/ngc598/www/triaris/web/dev.php(7): dmContext->dispatch()\n#18 {main}','dmPage','edit','http://triaris.dev/dev.php/+/dmPage/edit','dev','2014-09-13 16:16:44'),(2,'sfError404Exception','no current DmPage','no current DmPage\n#0 /home/ngc598/www/triaris/dm/dmFrontPlugin/modules/dmPage/actions/actions.class.php(32): sfAction->forward404Unless(NULL, \'no current DmPa...\')\n#1 /home/ngc598/www/triaris/dm/symfony/lib/action/sfActions.class.php(60): dmPageActions->executeEdit(Object(dmWebRequest))\n#2 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(92): sfActions->execute(Object(dmWebRequest))\n#3 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(78): sfExecutionFilter->executeAction(Object(dmPageActions))\n#4 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfExecutionFilter.class.php(42): sfExecutionFilter->handleAction(Object(sfFilterChain), Object(dmPageActions))\n#5 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfExecutionFilter->execute(Object(sfFilterChain))\n#6 /home/ngc598/www/triaris/dm/dmFrontPlugin/lib/filter/dmFrontInitFilter.php(34): sfFilterChain->execute()\n#7 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): dmFrontInitFilter->execute(Object(sfFilterChain))\n#8 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfBasicSecurityFilter.class.php(72): sfFilterChain->execute()\n#9 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfBasicSecurityFilter->execute(Object(sfFilterChain))\n#10 /home/ngc598/www/triaris/dm/dmCorePlugin/plugins/dmUserPlugin/lib/dmRememberMeFilter.class.php(56): sfFilterChain->execute()\n#11 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): dmRememberMeFilter->execute(Object(sfFilterChain))\n#12 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfRenderingFilter.class.php(33): sfFilterChain->execute()\n#13 /home/ngc598/www/triaris/dm/symfony/lib/filter/sfFilterChain.class.php(53): sfRenderingFilter->execute(Object(sfFilterChain))\n#14 /home/ngc598/www/triaris/dm/symfony/lib/controller/sfController.class.php(238): sfFilterChain->execute()\n#15 /home/ngc598/www/triaris/dm/dmCorePlugin/lib/controller/dmFrontWebController.php(38): sfController->forward(\'dmPage\', \'edit\')\n#16 /home/ngc598/www/triaris/dm/dmCorePlugin/lib/context/dmContext.php(290): dmFrontWebController->dispatch()\n#17 /home/ngc598/www/triaris/web/dev.php(7): dmContext->dispatch()\n#18 {main}','dmPage','edit','http://triaris.dev/dev.php/+/dmPage/edit','dev','2014-09-13 16:16:44');
/*!40000 ALTER TABLE `dm_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_group`
--

DROP TABLE IF EXISTS `dm_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_group`
--

LOCK TABLES `dm_group` WRITE;
/*!40000 ALTER TABLE `dm_group` DISABLE KEYS */;
INSERT INTO `dm_group` VALUES (1,'developer','Able to read and update source code','2014-09-08 00:32:08','2014-09-08 00:32:08'),(2,'seo','Seo knowledge','2014-09-08 00:32:08','2014-09-08 00:32:08'),(3,'integrator','Integrator','2014-09-08 00:32:08','2014-09-08 00:32:08'),(4,'webmaster 1','Webmaster level 1','2014-09-08 00:32:08','2014-09-08 00:32:08'),(5,'writer','Writer','2014-09-08 00:32:08','2014-09-08 00:32:08'),(6,'front_editor','Can fast edit front widgets','2014-09-08 00:32:08','2014-09-08 00:32:08');
/*!40000 ALTER TABLE `dm_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_group_permission`
--

DROP TABLE IF EXISTS `dm_group_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_group_permission` (
  `dm_group_id` bigint(20) NOT NULL DEFAULT '0',
  `dm_permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dm_group_id`,`dm_permission_id`),
  KEY `dm_group_permission_dm_permission_id_dm_permission_id` (`dm_permission_id`),
  CONSTRAINT `dm_group_permission_dm_group_id_dm_group_id` FOREIGN KEY (`dm_group_id`) REFERENCES `dm_group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_group_permission_dm_permission_id_dm_permission_id` FOREIGN KEY (`dm_permission_id`) REFERENCES `dm_permission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_group_permission`
--

LOCK TABLES `dm_group_permission` WRITE;
/*!40000 ALTER TABLE `dm_group_permission` DISABLE KEYS */;
INSERT INTO `dm_group_permission` VALUES (1,1),(2,2),(3,2),(4,2),(5,2),(3,5),(3,9),(4,9),(5,9),(3,10),(3,11),(3,12),(3,13),(3,14),(3,15),(6,16),(6,17),(6,18),(6,19),(6,20),(6,21),(6,22),(3,23),(3,24),(3,25),(2,26),(3,26),(4,26),(5,26),(3,27),(4,27),(5,27),(3,28),(2,30),(3,30),(4,30),(5,30),(2,31),(3,31),(4,31),(3,32),(4,32),(2,33),(3,33),(4,33),(2,34),(3,34),(4,34),(5,34),(3,35),(3,36),(2,37),(2,38),(2,39),(2,41),(2,42),(2,43),(2,44),(2,45),(4,47),(2,48),(3,48),(4,48),(5,48),(2,49),(3,49),(4,49),(5,49),(2,54),(3,54),(4,54),(3,55),(4,55),(3,56),(3,60),(3,61),(3,62),(3,63),(3,64);
/*!40000 ALTER TABLE `dm_group_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_layout`
--

DROP TABLE IF EXISTS `dm_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_layout` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'page',
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_layout`
--

LOCK TABLES `dm_layout` WRITE;
/*!40000 ALTER TABLE `dm_layout` DISABLE KEYS */;
INSERT INTO `dm_layout` VALUES (1,'Home','home','home'),(2,'FullWide','fullwide','fullwide'),(3,'LeftSide','leftside','leftside');
/*!40000 ALTER TABLE `dm_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_lock`
--

DROP TABLE IF EXISTS `dm_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_lock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `time` bigint(20) NOT NULL,
  `app` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `culture` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dmLockIndex_idx` (`user_id`,`module`,`action`,`record_id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `dm_lock_user_id_dm_user_id` FOREIGN KEY (`user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_lock`
--

LOCK TABLES `dm_lock` WRITE;
/*!40000 ALTER TABLE `dm_lock` DISABLE KEYS */;
INSERT INTO `dm_lock` VALUES (23,1,'admin','dmAdmin','index',0,1411236745,'admin','/admin.php','es');
/*!40000 ALTER TABLE `dm_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_mail_template`
--

DROP TABLE IF EXISTS `dm_mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_mail_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vars` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_mail_template`
--

LOCK TABLES `dm_mail_template` WRITE;
/*!40000 ALTER TABLE `dm_mail_template` DISABLE KEYS */;
INSERT INTO `dm_mail_template` VALUES (1,'dm_user_forgot_password','username, email, step2_url','2014-09-08 00:32:10','2014-09-08 00:32:10');
/*!40000 ALTER TABLE `dm_mail_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_mail_template_translation`
--

DROP TABLE IF EXISTS `dm_mail_template_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_mail_template_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `from_email` text COLLATE utf8_unicode_ci NOT NULL,
  `to_email` text COLLATE utf8_unicode_ci,
  `cc_email` text COLLATE utf8_unicode_ci,
  `bcc_email` text COLLATE utf8_unicode_ci,
  `reply_to_email` text COLLATE utf8_unicode_ci,
  `sender_email` text COLLATE utf8_unicode_ci,
  `list_unsuscribe` text COLLATE utf8_unicode_ci,
  `is_html` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_mail_template_translation_id_dm_mail_template_id` FOREIGN KEY (`id`) REFERENCES `dm_mail_template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_mail_template_translation`
--

LOCK TABLES `dm_mail_template_translation` WRITE;
/*!40000 ALTER TABLE `dm_mail_template_translation` DISABLE KEYS */;
INSERT INTO `dm_mail_template_translation` VALUES (1,'Sent to a user that requests a new password','Triaris: change your password','Hello %username%\nYou can choose a new password at %step2_url%','webmaster@domain.com','%email%',NULL,NULL,NULL,NULL,NULL,0,1,'es');
/*!40000 ALTER TABLE `dm_mail_template_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_media`
--

DROP TABLE IF EXISTS `dm_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_media_folder_id` bigint(20) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `dimensions` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `folderFile_idx` (`dm_media_folder_id`,`file`),
  KEY `dm_media_folder_id_idx` (`dm_media_folder_id`),
  CONSTRAINT `dm_media_dm_media_folder_id_dm_media_folder_id` FOREIGN KEY (`dm_media_folder_id`) REFERENCES `dm_media_folder` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_media`
--

LOCK TABLES `dm_media` WRITE;
/*!40000 ALTER TABLE `dm_media` DISABLE KEYS */;
INSERT INTO `dm_media` VALUES (1,3,'triaris-publicidad-guayaquil.png','image/png',6182,NULL,'2014-09-10 04:54:26','2014-09-10 04:54:26'),(2,3,'desarrollo-web-movil-guayaquil.png','image/png',144102,NULL,'2014-09-10 04:54:26','2014-09-10 04:54:26'),(3,3,'agencia-creativa-guayaquil.png','image/png',83998,NULL,'2014-09-10 04:54:26','2014-09-10 04:54:26'),(4,3,'triaris-disenio-y-publicidad-guayaquil.png','image/png',6125,NULL,'2014-09-14 07:32:59','2014-09-14 07:32:59'),(5,4,'Process-Risk-Solution-Ecuador.jpg','image/jpeg',28613,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(6,4,'rivalcorp-construccion-guayaquil.jpg','image/jpeg',7520,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(7,4,'fulgore-artesanias-bertha-serrano-guayaquil.jpg','image/jpeg',24299,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(8,4,'avconsa-empresa-de-construccion-guayaquil.jpg','image/jpeg',16454,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(9,4,'SFA-guayaquil-impresion-y-gigantografia.jpg','image/jpeg',10656,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(10,4,'prodiyen-materiales-de-construccion-ecuador.jpg','image/jpeg',30678,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(11,4,'forest-colors-design-consultoria-guayaquil.jpg','image/jpeg',32089,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(12,4,'alfadomus-adoquines-de-arcilla-guayaquil-ecuador.jpg','image/jpeg',15078,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(13,4,'RVA-tecnologia-ecuador.jpg','image/jpeg',29113,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(14,3,'publicidad-y-marketing-ecuador.png','image/png',16363,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(15,3,'los-mejores-sitios-web-joomla.png','image/png',15253,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(16,3,'triaris-publicidad-ecuador.png','image/png',6019,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57'),(17,3,'disenio-modelado-y-animacion-3d.png','image/png',17234,NULL,'2014-09-20 17:12:57','2014-09-20 17:12:57');
/*!40000 ALTER TABLE `dm_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_media_folder`
--

DROP TABLE IF EXISTS `dm_media_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_media_folder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rel_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `level` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rel_path` (`rel_path`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_media_folder`
--

LOCK TABLES `dm_media_folder` WRITE;
/*!40000 ALTER TABLE `dm_media_folder` DISABLE KEYS */;
INSERT INTO `dm_media_folder` VALUES (1,'',1,6,0),(3,'assets',2,3,1),(4,'client',4,5,1);
/*!40000 ALTER TABLE `dm_media_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_media_translation`
--

DROP TABLE IF EXISTS `dm_media_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_media_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `legend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_media_translation_id_dm_media_id` FOREIGN KEY (`id`) REFERENCES `dm_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_media_translation`
--

LOCK TABLES `dm_media_translation` WRITE;
/*!40000 ALTER TABLE `dm_media_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_media_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_page`
--

DROP TABLE IF EXISTS `dm_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `credentials` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `level` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `recordModuleAction_idx` (`module`,`action`,`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_page`
--

LOCK TABLES `dm_page` WRITE;
/*!40000 ALTER TABLE `dm_page` DISABLE KEYS */;
INSERT INTO `dm_page` VALUES (1,'main','root',0,NULL,1,6,0),(2,'main','error404',0,NULL,4,5,1),(3,'main','signin',0,NULL,2,3,1);
/*!40000 ALTER TABLE `dm_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_page_translation`
--

DROP TABLE IF EXISTS `dm_page_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_page_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_mod` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'snthdk',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_secure` tinyint(1) NOT NULL DEFAULT '0',
  `is_indexable` tinyint(1) NOT NULL DEFAULT '1',
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_page_translation_id_dm_page_id` FOREIGN KEY (`id`) REFERENCES `dm_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_page_translation`
--

LOCK TABLES `dm_page_translation` WRITE;
/*!40000 ALTER TABLE `dm_page_translation` DISABLE KEYS */;
INSERT INTO `dm_page_translation` VALUES (1,'','Inicio','Inicio',NULL,'Diseño y Publicidad Guayaquil, Diseño y desarrollo de Sitios Webs administrables en Dm, Joomla y Wordpress,\nCampañas Digitales y de Social Media para de Facebook, Twitter, Instagram, Google Plus, Whatsapp.\nDesarrollo de aplicaciones web móviles en Ecuador','Diseño y Publicidad Guayaquil,  Sitios Webs Guayaquil, DM,Joomla y Wordpress, Campañas Digitales, Social Media, Facebook, Twitter, Instagram, Google Plus, WhatsApp.\nAplicaciones web móvil en Ecuador.\nDiseño 3D, Exhibidores, Stands.','snthdk',1,0,1,'es'),(2,'error404.html','Pägina no Encontrada','Pägina no Encontrada',NULL,'Mil Disculpas, al parecer el enlace que buscabas ya no se encuentra disponible, contáctate con nosotros o en su defecto revisa nuestras categorias en Diseño y Publicidad Guayaquil, Diseño y desarrollo de Sitios Webs administrables en Dm, Joomla y Wordpres',NULL,'snthdk',1,0,1,'es'),(3,'security/signin.html','Ingreso de Usuario','Ingreso de Usuario',NULL,NULL,NULL,'snthdk',1,0,1,'es');
/*!40000 ALTER TABLE `dm_page_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_page_view`
--

DROP TABLE IF EXISTS `dm_page_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_page_view` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `dm_layout_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dmPageViewModuleAction_idx` (`module`,`action`),
  KEY `dm_layout_id_idx` (`dm_layout_id`),
  CONSTRAINT `dm_page_view_dm_layout_id_dm_layout_id` FOREIGN KEY (`dm_layout_id`) REFERENCES `dm_layout` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_page_view`
--

LOCK TABLES `dm_page_view` WRITE;
/*!40000 ALTER TABLE `dm_page_view` DISABLE KEYS */;
INSERT INTO `dm_page_view` VALUES (1,'main','root',1),(2,'main','error404',1),(3,'main','signin',1);
/*!40000 ALTER TABLE `dm_page_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_permission`
--

DROP TABLE IF EXISTS `dm_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_permission`
--

LOCK TABLES `dm_permission` WRITE;
/*!40000 ALTER TABLE `dm_permission` DISABLE KEYS */;
INSERT INTO `dm_permission` VALUES (1,'system','System administrator','2014-09-08 00:32:08','2014-09-08 00:32:08'),(2,'admin','Log into administration','2014-09-08 00:32:08','2014-09-08 00:32:08'),(3,'clear_cache','Clear the cache','2014-09-08 00:32:08','2014-09-08 00:32:08'),(4,'log','Manage logs','2014-09-08 00:32:08','2014-09-08 00:32:08'),(5,'code_editor','Use admin and front code editors','2014-09-08 00:32:08','2014-09-08 00:32:08'),(6,'security_user','Manage security users','2014-09-08 00:32:08','2014-09-08 00:32:08'),(7,'security_permission','Manage security permissions','2014-09-08 00:32:08','2014-09-08 00:32:08'),(8,'security_group','Manage security groups','2014-09-08 00:32:08','2014-09-08 00:32:08'),(9,'content','CRUD dynamic content in admin','2014-09-08 00:32:08','2014-09-08 00:32:08'),(10,'zone_add','Add zones','2014-09-08 00:32:08','2014-09-08 00:32:08'),(11,'zone_edit','Edit zones','2014-09-08 00:32:08','2014-09-08 00:32:08'),(12,'zone_delete','Delete zones','2014-09-08 00:32:08','2014-09-08 00:32:08'),(13,'widget_add','Add widgets','2014-09-08 00:32:08','2014-09-08 00:32:08'),(14,'widget_edit','Edit widgets','2014-09-08 00:32:08','2014-09-08 00:32:08'),(15,'widget_delete','Delete widgets','2014-09-08 00:32:08','2014-09-08 00:32:08'),(16,'widget_edit_fast','Can fast edit widgets','2014-09-08 00:32:08','2014-09-08 00:32:08'),(17,'widget_edit_fast_record','Fast edit widget record','2014-09-08 00:32:08','2014-09-08 00:32:08'),(18,'widget_edit_fast_content_title','Fast edit widget content title','2014-09-08 00:32:08','2014-09-08 00:32:08'),(19,'widget_edit_fast_content_link','Fast edit widget content link','2014-09-08 00:32:08','2014-09-08 00:32:08'),(20,'widget_edit_fast_content_image','Fast edit widget content image','2014-09-08 00:32:08','2014-09-08 00:32:08'),(21,'widget_edit_fast_content_text','Fast edit widget content text','2014-09-08 00:32:08','2014-09-08 00:32:08'),(22,'widget_edit_fast_navigation_menu','Fast edit widget navigation menu','2014-09-08 00:32:08','2014-09-08 00:32:08'),(23,'page_add','Add pages','2014-09-08 00:32:08','2014-09-08 00:32:08'),(24,'page_edit','Edit pages','2014-09-08 00:32:08','2014-09-08 00:32:08'),(25,'page_delete','Delete pages','2014-09-08 00:32:08','2014-09-08 00:32:08'),(26,'page_bar_admin','See page bar in admin','2014-09-08 00:32:08','2014-09-08 00:32:08'),(27,'media_bar_admin','See media bar in admin','2014-09-08 00:32:08','2014-09-08 00:32:08'),(28,'media_library','Use media library in admin','2014-09-08 00:32:08','2014-09-08 00:32:08'),(29,'media_ignore_whitelist','Upload media with any filetype','2014-09-08 00:32:08','2014-09-08 00:32:08'),(30,'tool_bar_admin','See toolbar in admin','2014-09-08 00:32:08','2014-09-08 00:32:08'),(31,'page_bar_front','See page bar in front','2014-09-08 00:32:08','2014-09-08 00:32:08'),(32,'media_bar_front','See media bar in front','2014-09-08 00:32:08','2014-09-08 00:32:08'),(33,'tool_bar_front','See toolbar in front','2014-09-08 00:32:08','2014-09-08 00:32:08'),(34,'site_view','See non-public website and inactive pages','2014-09-08 00:32:08','2014-09-08 00:32:08'),(35,'loremize','Create automatic random content','2014-09-08 00:32:08','2014-09-08 00:32:08'),(36,'export_table','Export table contents','2014-09-08 00:32:08','2014-09-08 00:32:08'),(37,'sitemap','Regenerate sitemap','2014-09-08 00:32:08','2014-09-08 00:32:08'),(38,'automatic_metas','Configure automatic pages metas','2014-09-08 00:32:08','2014-09-08 00:32:08'),(39,'manual_metas','Configure manually pages metas','2014-09-08 00:32:08','2014-09-08 00:32:08'),(40,'manage_pages','Move and sort pages','2014-09-08 00:32:08','2014-09-08 00:32:08'),(41,'url_redirection','Configure url redirections','2014-09-08 00:32:08','2014-09-08 00:32:08'),(42,'use_google_analytics','Use google analytics','2014-09-08 00:32:08','2014-09-08 00:32:08'),(43,'google_analytics','Configure google analytics','2014-09-08 00:32:08','2014-09-08 00:32:08'),(44,'use_google_webmaster_tools','Use google webmaster tools','2014-09-08 00:32:08','2014-09-08 00:32:08'),(45,'google_webmaster_tools','Configure google webmaster tools','2014-09-08 00:32:08','2014-09-08 00:32:08'),(46,'xiti','Configure Xiti','2014-09-08 00:32:08','2014-09-08 00:32:08'),(47,'search_engine','Manage internal search engine','2014-09-08 00:32:08','2014-09-08 00:32:08'),(48,'see_log','See the logs','2014-09-08 00:32:08','2014-09-08 00:32:08'),(49,'see_chart','See the charts','2014-09-08 00:32:08','2014-09-08 00:32:08'),(50,'see_diagrams','See the developer diagrams','2014-09-08 00:32:08','2014-09-08 00:32:08'),(51,'see_server','See the server infos','2014-09-08 00:32:08','2014-09-08 00:32:08'),(52,'see_request','See the requests window','2014-09-08 00:32:08','2014-09-08 00:32:08'),(53,'see_event','See the events window','2014-09-08 00:32:08','2014-09-08 00:32:08'),(54,'config_panel','Use the configuration panel','2014-09-08 00:32:08','2014-09-08 00:32:08'),(55,'translation','Use the translation interface','2014-09-08 00:32:08','2014-09-08 00:32:08'),(56,'layout','Use the layout interface','2014-09-08 00:32:08','2014-09-08 00:32:08'),(57,'sent_mail','See mails sent by server','2014-09-08 00:32:08','2014-09-08 00:32:08'),(58,'mail_template','Configure mail templates','2014-09-08 00:32:08','2014-09-08 00:32:08'),(59,'error_log','See error log','2014-09-08 00:32:08','2014-09-08 00:32:08'),(60,'interface_settings','Manage interface settings like default image resize method','2014-09-08 00:32:08','2014-09-08 00:32:08'),(61,'behavior_add','Add behaviors to page elements','2014-09-08 00:32:08','2014-09-08 00:32:08'),(62,'behavior_edit','Edit settings of the behaviors','2014-09-08 00:32:08','2014-09-08 00:32:08'),(63,'behavior_delete','Delete attached behaviors','2014-09-08 00:32:08','2014-09-08 00:32:08'),(64,'behavior_sort','Sort the sequence of the behaviors','2014-09-08 00:32:08','2014-09-08 00:32:08');
/*!40000 ALTER TABLE `dm_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission`
--

DROP TABLE IF EXISTS `dm_record_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `secure_module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secure_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secure_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secure_record` bigint(20) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission`
--

LOCK TABLES `dm_record_permission` WRITE;
/*!40000 ALTER TABLE `dm_record_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission_association`
--

DROP TABLE IF EXISTS `dm_record_permission_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_secure_action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dm_secure_module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dm_secure_model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission_association`
--

LOCK TABLES `dm_record_permission_association` WRITE;
/*!40000 ALTER TABLE `dm_record_permission_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission_association_group`
--

DROP TABLE IF EXISTS `dm_record_permission_association_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission_association_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_group_id` bigint(20) NOT NULL,
  `dm_record_permission_association_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_record_permission_association_id_idx` (`dm_record_permission_association_id`),
  KEY `dm_group_id_idx` (`dm_group_id`),
  CONSTRAINT `dddi` FOREIGN KEY (`dm_record_permission_association_id`) REFERENCES `dm_record_permission_association` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_record_permission_association_group_dm_group_id_dm_group_id` FOREIGN KEY (`dm_group_id`) REFERENCES `dm_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission_association_group`
--

LOCK TABLES `dm_record_permission_association_group` WRITE;
/*!40000 ALTER TABLE `dm_record_permission_association_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission_association_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission_association_user`
--

DROP TABLE IF EXISTS `dm_record_permission_association_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission_association_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_user_id` bigint(20) NOT NULL,
  `dm_record_permission_association_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_record_permission_association_id_idx` (`dm_record_permission_association_id`),
  KEY `dm_user_id_idx` (`dm_user_id`),
  CONSTRAINT `dddi_2` FOREIGN KEY (`dm_record_permission_association_id`) REFERENCES `dm_record_permission_association` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_record_permission_association_user_dm_user_id_dm_user_id` FOREIGN KEY (`dm_user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission_association_user`
--

LOCK TABLES `dm_record_permission_association_user` WRITE;
/*!40000 ALTER TABLE `dm_record_permission_association_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission_association_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission_group`
--

DROP TABLE IF EXISTS `dm_record_permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission_group` (
  `dm_group_id` bigint(20) NOT NULL DEFAULT '0',
  `dm_record_permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dm_group_id`,`dm_record_permission_id`),
  KEY `dddi_4` (`dm_record_permission_id`),
  CONSTRAINT `dddi_4` FOREIGN KEY (`dm_record_permission_id`) REFERENCES `dm_record_permission` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_record_permission_group_dm_group_id_dm_group_id` FOREIGN KEY (`dm_group_id`) REFERENCES `dm_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission_group`
--

LOCK TABLES `dm_record_permission_group` WRITE;
/*!40000 ALTER TABLE `dm_record_permission_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_record_permission_user`
--

DROP TABLE IF EXISTS `dm_record_permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_record_permission_user` (
  `dm_user_id` bigint(20) NOT NULL DEFAULT '0',
  `dm_record_permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dm_user_id`,`dm_record_permission_id`),
  KEY `dddi_6` (`dm_record_permission_id`),
  CONSTRAINT `dddi_6` FOREIGN KEY (`dm_record_permission_id`) REFERENCES `dm_record_permission` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_record_permission_user_dm_user_id_dm_user_id` FOREIGN KEY (`dm_user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_record_permission_user`
--

LOCK TABLES `dm_record_permission_user` WRITE;
/*!40000 ALTER TABLE `dm_record_permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_record_permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_redirect`
--

DROP TABLE IF EXISTS `dm_redirect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_redirect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_redirect`
--

LOCK TABLES `dm_redirect` WRITE;
/*!40000 ALTER TABLE `dm_redirect` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_redirect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_remember_key`
--

DROP TABLE IF EXISTS `dm_remember_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_remember_key` (
  `dm_user_id` bigint(20) DEFAULT NULL,
  `remember_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`ip_address`),
  KEY `dm_user_id_idx` (`dm_user_id`),
  CONSTRAINT `dm_remember_key_dm_user_id_dm_user_id` FOREIGN KEY (`dm_user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_remember_key`
--

LOCK TABLES `dm_remember_key` WRITE;
/*!40000 ALTER TABLE `dm_remember_key` DISABLE KEYS */;
INSERT INTO `dm_remember_key` VALUES (1,'037cd92d8370ef5b4278818024f5a366','192.168.1.101','2014-09-20 17:12:38');
/*!40000 ALTER TABLE `dm_remember_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_sent_mail`
--

DROP TABLE IF EXISTS `dm_sent_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_sent_mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_mail_template_id` bigint(20) DEFAULT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `from_email` text COLLATE utf8_unicode_ci NOT NULL,
  `to_email` text COLLATE utf8_unicode_ci,
  `cc_email` text COLLATE utf8_unicode_ci,
  `bcc_email` text COLLATE utf8_unicode_ci,
  `reply_to_email` text COLLATE utf8_unicode_ci,
  `sender_email` text COLLATE utf8_unicode_ci,
  `strategy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transport` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `culture` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debug_string` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_mail_template_id_idx` (`dm_mail_template_id`),
  CONSTRAINT `dm_sent_mail_dm_mail_template_id_dm_mail_template_id` FOREIGN KEY (`dm_mail_template_id`) REFERENCES `dm_mail_template` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_sent_mail`
--

LOCK TABLES `dm_sent_mail` WRITE;
/*!40000 ALTER TABLE `dm_sent_mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_sent_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_setting`
--

DROP TABLE IF EXISTS `dm_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `params` text COLLATE utf8_unicode_ci,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `credentials` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_setting`
--

LOCK TABLES `dm_setting` WRITE;
/*!40000 ALTER TABLE `dm_setting` DISABLE KEYS */;
INSERT INTO `dm_setting` VALUES (1,'site_name','text',NULL,'site',NULL),(2,'site_active','boolean',NULL,'site',NULL),(3,'site_indexable','boolean',NULL,'site',NULL),(4,'site_working_copy','boolean',NULL,'site',NULL),(5,'ga_key','text',NULL,'tracking','google_analytics'),(6,'ga_demographics','boolean',NULL,'tracking','google_analytics'),(7,'ga_token','text',NULL,'internal','google_analytics'),(8,'gwt_key','text',NULL,'tracking','google_webmaster_tools'),(9,'xiti_code','textarea',NULL,'tracking','xiti'),(10,'search_stop_words','textarea',NULL,'search engine','search_engine'),(11,'base_urls','textarea',NULL,'internal','system'),(12,'image_resize_method','select','fit=Fit scale=Scale inflate=Inflate top=Top right=Right left=Left bottom=Bottom center=Center','interface','interface_settings'),(13,'image_resize_quality','number',NULL,'interface','interface_settings'),(14,'link_external_blank','boolean',NULL,'interface','interface_settings'),(15,'link_current_span','boolean',NULL,'interface','interface_settings'),(16,'link_use_page_title','boolean',NULL,'interface','interface_settings'),(17,'title_prefix','text',NULL,'seo','manual_metas'),(18,'title_suffix','text',NULL,'seo','manual_metas'),(19,'smart_404','boolean',NULL,'seo','url_redirection'),(20,'colorbox_use_theme','boolean',NULL,'Colorbox',NULL),(21,'colorbox_theme','select','theme1=Theme 1 theme2=Theme 2 theme3=Theme 3 theme4=Theme 4 theme5=Theme 5','Colorbox',NULL);
/*!40000 ALTER TABLE `dm_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_setting_translation`
--

DROP TABLE IF EXISTS `dm_setting_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_setting_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `default_value` text COLLATE utf8_unicode_ci,
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_setting_translation_id_dm_setting_id` FOREIGN KEY (`id`) REFERENCES `dm_setting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_setting_translation`
--

LOCK TABLES `dm_setting_translation` WRITE;
/*!40000 ALTER TABLE `dm_setting_translation` DISABLE KEYS */;
INSERT INTO `dm_setting_translation` VALUES (1,'The site name','Triaris','Triaris','es'),(2,'Is the site ready for visitors ?','1','1','es'),(3,'Is the site ready for search engine crawlers ?','1','1','es'),(4,'Is this site the current working copy ?','1','1','es'),(5,'The google analytics key without javascript stuff ( e.g. UA-9876614-1 )','UA-28161430-1','','es'),(6,'Enable Demographic tracking in GA code','1','','es'),(7,'Auth token gor Google Analytics, computed from password','DQAAAPgAAABmXCEEQJS3gzQahCUlDTca77DdsD-oLaVsXtRX-wrU5cp9Erv6zrySLaVaJMO-ZveyQXsq_nVDBKMzl4h94VyX_JmQZcVnaNF6AoIMOSZd8d1iV1LgJglWBDwDSie-ASvOPTFh1PHBLDHO5qcF0mYnA_aOUiEFJaHpcuoVWivDJfLUsZx52pgEm0fPdhk4XeFu72KdTc66lCgIMb2re8jxZcrQPPB4Qqv3irPGNAeYui-tdavbvPDD_Rq16mt7OPZq7BYoDmHZc0ftQeAroMC7fFnCvfz3ayi5MZN5AwsChKjzPgYOeBMH6TuHCBUAbAHMk7sQeWBVjxnM3QoK9jGV','','es'),(8,'The google webmaster tools filename without google and .html ( e.g. a913b555ba9b4f13 )','0bb97ea6dad14878','','es'),(9,'The xiti html code',NULL,'','es'),(10,'Words to exclude from searches (e.g. the, a, to )',NULL,'','es'),(11,'Diem base urls for different applications/environments/cultures','{\"front-prod\":\"http:\\/\\/triaris.dev\\/index.php\",\"admin-prod\":\"http:\\/\\/triaris.dev\\/admin.php\",\"admin-dev\":\"http:\\/\\/triaris.dev\\/admin_dev.php\",\"front-dev\":\"http:\\/\\/triaris.dev\\/dev.php\"}','','es'),(12,'Default method when an image needs to be resized','center','center','es'),(13,'Jpeg default quality when generating thumbnails','95','95','es'),(14,'Links to other domain get automatically a _blank target',NULL,'0','es'),(15,'Links to current page are changed from <a> to <span>','1','1','es'),(16,'Add an automatic title on link based on the target page title','1','1','es'),(17,'Append something at the beginning of all pages title','Triaris Design Studio |  ','','es'),(18,'Append something at the end of all pages title','  | Soluciones en Publicidad, Diseño Web y 3D en Guayaquil - Ecuador',' | Triaris','es'),(19,'When a page is not found, user is redirect to a similar page. The internal search index is used to find the best page for requested url.','1','1','es'),(20,'Use a predefined colorbox theme','1','1','es'),(21,NULL,'theme1','theme1','es');
/*!40000 ALTER TABLE `dm_setting_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_trans_unit`
--

DROP TABLE IF EXISTS `dm_trans_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_trans_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_catalogue_id` bigint(20) NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `target` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_catalogue_id_idx` (`dm_catalogue_id`),
  CONSTRAINT `dm_trans_unit_dm_catalogue_id_dm_catalogue_id` FOREIGN KEY (`dm_catalogue_id`) REFERENCES `dm_catalogue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_trans_unit`
--

LOCK TABLES `dm_trans_unit` WRITE;
/*!40000 ALTER TABLE `dm_trans_unit` DISABLE KEYS */;
INSERT INTO `dm_trans_unit` VALUES (1,2,'Open','Abrir',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(2,2,'Delete','Borrar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(3,2,'Add','Añadir',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(4,2,'Add a %1%','Añadir un %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(5,2,'Cancel','Cancelar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(6,2,'Are you sure?','¿Está seguro?',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(7,2,'Unassociated','No Asociado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(8,2,'Associated','Asociado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(9,2,'Forgot your password?','¿Olvidó su contraseña?',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(10,2,'yes','sí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(11,2,'no','no',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(12,2,'yes or no','sí o no',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(13,2,'Next','Siguiente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(14,2,'Previous','Anterior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(15,2,'First','Primero',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(16,2,'Last','Último',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(17,2,'Is active','Está activo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(18,2,'Is active:','Está activo:',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(19,2,'Updated at','Actualizado el',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(20,2,'Updated at:','Actualizado el:',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(21,2,'Created at','Creado el',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(22,2,'Created at:','Creado el:',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(23,2,'Created by','Creado por',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(24,2,'Updated by','Actualizado por',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(25,2,'Query','Consulta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(26,2,'Image alt','Texto de Imagen',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(27,2,'Image alt:','Texto de Imagen:',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(28,2,'Your modifications have been saved','Las modificaciones han sido guardadas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(29,2,'Required','Requerido',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(30,2,'Content','Contenido',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(31,2,'Home','Inicio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(32,2,'Tools','Herramientas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(33,2,'System','Sistema',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(34,2,'Update project','Actualizar proyecto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(35,2,'Project successfully updated','Proyecto actualizado correctamente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(36,2,'Search','Búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(37,2,'Search in %1%','Buscar en %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(38,2,'Cancel search','Cancelar la búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(39,2,'Active search','Búsqueda activa',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(40,2,'is empty','está vacído',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(41,2,'from','desde',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(42,2,'to','hasta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(43,2,'[0] no result|[1] 1 result|(1,+Inf] %1% results','[0] ningún resultado | [1] 1 resultado | (1,+Inf] %1% de resultados',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(44,2,'Choose an action','Elegir una acción',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(45,2,'Edit','Editar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(46,2,'New','Nuevo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(47,2,'You must at least select one item.','Debe elegir por lo menos un elemento.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(48,2,'Filter','Filtro',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(49,2,'Reset','Resetear',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(50,2,'Add a','Añadir uno',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(51,2,'The item was updated successfully.','El elemento se ha actualizado con éxito.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(52,2,'The item has not been saved due to some errors.','El elemento no se ha guardado debido a algunos errores.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(53,2,'Invalid.','Inválido.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(54,2,'Required.','Obligatorio.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(55,2,'The item was updated successfully. You can add another one below.','El elemento se ha actualizado con éxito. Puede agregar otro debajo.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(56,2,'Translation','Traducción',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(57,2,'Sentences','Frases',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(58,2,'Loremize','Loremizar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(59,2,'does not match the date format','no coincide con el formato de fecha',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(60,2,'The date must be before','La fecha debe ser anterior a',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(61,2,'The date must be after','La fecha debe ser posterior a',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(62,2,'[0]no element|[1]1 element|(1,+Inf]%1% elements','[0] no hay ningún elemento | [1] 1 elemento | (1,+Inf] %1% elementos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(63,2,'You must select an action to execute on the selected items.','Debe seleccionar una acción a ejecutar con los elementos seleccionados.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(64,2,'No result','Ningún resultado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(65,2,'The selected items have been deleted successfully.','Los elementos seleccionados se han eliminado con éxito.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(66,2,'The selected items have been modified successfully','Los elementos seleccionados se han modificado con éxito',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(67,2,'A problem occurs when modifying the selected items','Han ocurrido problema(s) al  modificar los elementos seleccionados',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(68,2,'Root','Raíz',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(69,2,'Page not found','Página no encontrada',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(70,2,'Media library','Librería de Medias',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(71,2,'UML Model Diagram','Diagrama de modelos UML',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(72,2,'The item was deleted successfully.','El elemento fue eliminado con éxito.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(73,2,'element','elemento',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(74,2,'elements','elementos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(75,2,'Add a folder here','Añadir una carpeta aquí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(76,2,'Import from a zip','Importar desde un archivo ZIP',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(77,2,'Add some files here','Añadir archivos aquí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(78,2,'Add a file here','Añadir archivo aquí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(79,2,'Clear cache','Borrar la caché',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(80,2,'Clear media cache','Borrar caché de media',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(81,2,'Name','Nombre',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(82,2,'Rename this folder','Cambiar el nombre de esta carpeta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(83,2,'Move this folder','Mover esta carpeta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(84,2,'Delete this folder','Eliminar esta carpeta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(85,2,'Already exists in this folder','Ya existe en esta carpeta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(86,2,'This is a bad name','Nombre incorrecto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(87,2,'File','Archivo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(88,2,'Thumbnails can not be created in %1%','No se pueden crear thumbnails en %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(89,2,'Folder %1% is not writable','La carpeta %1% no tiene permisos de escritura',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(90,2,'File %1% is not writable','El archivo %1% no tiene permisos de escritura',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(91,2,'This folder is not writable','Esta carpeta no tiene permisos de escritura',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(92,2,'This file is not writable','Este archivo no tiene permisos de escritura',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(93,2,'Size','Tamaño',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(94,2,'Legend','Leyenda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(95,2,'Author','Autor',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(96,2,'License','Licencia',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(97,2,'Validate','Validar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(98,2,'Save','Guardar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(99,2,'Save and Add','Guardar y añadir',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(100,2,'Save and Next','Guardar y Siguiente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(101,2,'Save and Back to list','Guardar y volver a la lista',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(102,2,'Save modifications','Guardar modificaciones',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(103,2,'Delete this file','Eliminar este archivo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(104,2,'Close','Cerrar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(105,2,'Delete this %1%','Eliminar este %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(106,2,'Delete this page','Borrar esta página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(107,2,'Active','Activo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(108,2,'Referers','Referentes',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(109,2,'Administration login','Login de administración',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(110,2,'Login','Iniciar sesión',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(111,2,'Username','Nombre de usuario',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(112,2,'Password','Contraseña',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(113,2,'Password (again)','Contraseña (otra vez)',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(114,2,'Remember','Recordar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(115,2,'The username and/or password is invalid.','El nombre de usuario y/o la contraseña no es válida.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(116,2,'Profiles','Perfiles',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(117,2,'Profile','Perfil',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(118,2,'User','Usuario',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(119,2,'Users','Usuarios',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(120,2,'Metas','Metas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(121,2,'Automatic Page','Página automática',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(122,2,'Automatic Pages','Páginas automática',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(123,2,'Site tree','Árbol de la web',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(124,2,'Some SEO improvements should be applied','Se deberían aplicar algunas mejoras SEO',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(125,2,'Some page have the same url','Algunas páginas tienen la misma URL',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(126,2,'Click here to see them','Haga clic aquí para verlos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(127,2,'Seo improvements','Mejoras Seo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(128,2,'Duplicated %1%','Duplicado %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(129,2,'Automatic page','Página automática',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(130,2,'Manual page','Página manual',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(131,2,'Modify object','Modificar objeto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(132,2,'Configure automatic seo for %1% pages','Configurar seo automático para %1% páginas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(133,2,'View page on website','Ver página en el sitio web',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(134,2,'Edit image','Editar imagen',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(135,2,'Loading...','Cargando ...',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(136,2,'Preview','Previsualización',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(137,2,'Back to list','Volver a la lista',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(138,2,'Back to %1% list','Volver a la lista %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(139,2,'The form submission cannot be processed. It probably means that you have uploaded a file that is too big.','El envío del formulario no puede ser procesado. Probablemente significa que ha subido un archivo que es demasiado grande.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(140,2,'A problem occurs when deleting the selected items.','Se produce un problema al eliminar los elementos seleccionados.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(141,2,'elements per page','elementos por página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(142,2,'Sort','Ordenar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(143,2,'Sort %1%','Ordenar %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(144,2,'Sort %1% for %2%','Ordenar %1% para %2%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(145,2,'Drag & drop elements, then','Arrastre y suelte los elementos, ahí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(146,2,'A problem occured when sorting the items','Ocurrió un problema al ordenar los elementos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(147,2,'The items have been sorted successfully','Los ítemes han sido ordenados con éxito',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(148,2,'Add a zone','Agregar a la zona',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(149,2,'Add a widget','Añadir un widget',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(150,2,'Go to site','Ir al sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(151,2,'You don\'t have the required permission to access this page.','Usted no tiene el permiso necesario para acceder a esta página.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(152,2,'Edit this zone','Editar esta zona',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(153,2,'Delete this zone','Eliminar esta zona',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(154,2,'CSS class','Clase CSS',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(155,2,'Width','Ancho',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(156,2,'Height','Alto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(157,2,'Edit this %1%','Editar %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(158,2,'Delete this widget','Eliminar este widget',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(159,2,'Title','Título',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(160,2,'Link','Vínculo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(161,2,'Text','Texto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(162,2,'Advanced','Avanzado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(163,2,'Bread crumb','Bread crumb',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(164,2,'Search form','Formulario de búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(165,2,'Search results','Resultados de la búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(166,2,'Go to content','Ir al contenido',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(167,2,'New widget','Nuevo widget',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(168,2,'Use media','Usar medio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(169,2,'Or upload a file','O suba un archivo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(170,2,'Drag & drop a media here','Arrastre y suelte un medio aquí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(171,2,'Media','Medio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(172,2,'You must use a media or upload a file','Debe utilizar un medio o cargar un archivo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(173,2,'Try','Intentar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(174,2,'Change file','Cambiar el archivo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(175,2,'Method','Método',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(176,2,'Center','Centro',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(177,2,'Scale','Escala',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(178,2,'Inflate','Inflar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(179,2,'Fit','Ajustar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(180,2,'Save and close','Guardar y cerrar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(181,2,'This size is not valid.','Este tamaño no es válida.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(182,2,'This color is not valid.','Este color no es válido.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(183,2,'Per page','Por página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(184,2,'First page','Primera página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(185,2,'Previous page','Página Anterior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(186,2,'Next page','Página siguiente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(187,2,'Last page','Última página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(188,2,'Top','Superior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(189,2,'Bottom','Inferior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(190,2,'View','Ver',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(191,2,'Order by','Ordenar por',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(192,2,'Random','Aleatoriamente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(193,2,'automatic','automático',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(194,2,'Add new page','Añadir página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(195,2,'Seo','Seo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(196,2,'Integration','Integración',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(197,2,'Edit page','Editar página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(198,2,'Logout','Cerrar sesión',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(199,2,'Export in %1%','Exportar en %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(200,2,'Available','Disponible',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(201,2,'Requires authentication','Requiere autentificación',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(202,2,'Search engine crawlers','Rastreadores de motores de búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(203,2,'Show page structure','Mostrar página (sale de modo de edición)',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(204,2,'Generate sitemap','Generar el mapa del sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(205,2,'Links','Enlaces',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(206,2,'Act on selection','Actuar al seleccionar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(207,2,'Activate','Activar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(208,2,'Deactivate','Desactivar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(209,2,'Remove','Eliminar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(210,2,'Sorry, but you can not access administration with your current browser','Lo sentimos, pero no se puede acceder a la administración con su navegador actual',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(211,2,'Select all','Seleccionar todos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(212,2,'Unselect all','Deseleccionar todos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(213,2,'Configuration panel','Panel de configuración',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(214,2,'This is not a valid hexadecimal color','Este no es un color hexadecimal válido',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(215,2,'%1% - %2% of %3%',' %1% - %2% en %3%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(216,2,'Display','Mostrar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(217,2,'User logged in','Usuario conectado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(218,2,'User logged out','Usuario se desconectó',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(219,2,'Cache cleared','Cache vaciado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(220,2,'This Week','Esta Semana',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(221,2,'This Year','Este Año',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(222,2,'Activity','Actividad',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(223,2,'Server','Servidor',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(224,2,'Browser','Navegador',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(225,2,'Browsers','Navegadores',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(226,2,'Location','Ubicación',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(227,2,'Events','Eventos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(228,2,'Requests','Petición',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(229,2,'Subject','Sujeto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(230,2,'Expanded view','Vista expandida',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(231,2,'Show','Mostrar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(232,2,'Gallery','Galería',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(233,2,'Edit medias','Editar medios',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(234,2,'Remove this media','Eliminar este medio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(235,2,'Preferences','Preferencias',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(236,2,'Mail templates','Plantillas de correo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(237,2,'Layouts','Diseños',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(238,2,'Zone','Zona',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(239,2,'Navigation','Navegación',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(240,2,'Configuration','Configuración',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(241,2,'Chart','Gráfico',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(242,2,'Log','Registro',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(243,2,'Monitoring','Monitoreo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(244,2,'Manage index','Manejar índice',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(245,2,'Search engine','Motor de búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(246,2,'Google analytics','Google analytics',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(247,2,'Google webmaster tools','Herramientas para webmasters de Google',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(248,2,'Services','Servicios',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(249,2,'Sitemap','Mapa del sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(250,2,'Url Redirections','Redirecciones Url',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(251,2,'Redirections','Redirecciones',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(252,2,'Settings','Configuraciones',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(253,2,'Sent mails','Correos electronicos enviados',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(254,2,'Errors','Errores',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(255,2,'Permissions','Permisos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(256,2,'Groups','Grupos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(257,2,'Security','Seguridad',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(258,2,'See diagrams','Ver diagramas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(259,2,'Code Editor','Editor de código',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(260,2,'Diem Console','Consola Diem',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(261,2,'Dev','Desarrollo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(262,2,'Exception','Excepción',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(263,2,'Cache','Caché',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(264,2,'This chart is not available.','Este gráfico no está disponible.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(265,2,'Dm gallery','Galería Diem',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(266,2,'Email','Correo electrónico',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(267,2,'Last login','Última entrada',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(268,2,'Visible','Visible',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(269,2,'External services','Servicios externos',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(270,2,'Interface','Interfaz',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(271,2,'Site','Sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(272,2,'Tracking','Rastreo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(273,2,'Right','Derecho',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(274,2,'Left','Izquierdo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(275,2,'Image resize method','Método para redimensionar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(276,2,'Default method when an image needs to be resized','Método por defecto cuando una imagen necesita ser redimensionada',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(277,2,'Image resize quality','Calidad de imagen al redimensionar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(278,2,'Jpeg default quality when generating thumbnails','Calidad Jpeg  por defecto al generar thumbnails',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(279,2,'Link external blank','Enlace externo en blanco',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(280,2,'Links to other domain get automatically a _blank target','Enlaces a otro dominio automáticamente reciben un blanco _blank',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(281,2,'Link current span','Enlace a span actual',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(282,2,'Links to current page are changed from <a> to <span>','Enlaces a la página actual se cambian de <a> a <span>',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(283,2,'Link use page title','Enlace usa título de página',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(284,2,'Add an automatic title on link based on the target page title','Agregar título automático a enlace basado en el título de la página de destino',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(285,2,'Search stop words','Stop words de la búsqueda',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(286,2,'Words to exclude from searches (e.g. the, a, to )','Palabras para excluir de las búsquedas (por ejemplo, la, a, de)',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(287,2,'Title prefix','Prefijo Título',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(288,2,'Append something at the beginning of all pages title','Añadir algo al principio de todos los títulos de páginas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(289,2,'Title suffix','Sufijo de título',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(290,2,'Append something at the end of all pages title','Añadir algo al final de todos los títulos de páginas',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(291,2,'Smart 404','404 inteligente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(292,2,'When a page is not found, user is redirect to a similar page. The internal search index is used to find the best page for requested url.','Cuando no se encuentre una página, el usuario es redireccionado a una página similar. Se usa el índice de búsqueda interno para encontrar la página más adecuada para la url pedida.',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(293,2,'Site name','Nombre del sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(294,2,'The site name','El nombre del sitio',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(295,2,'Site active','Site activo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(296,2,'Is the site ready for visitors ?','¿Está listo el site para los visitantes?',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(297,2,'Site indexable','Site indexable',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(298,2,'Is the site ready for search engine crawlers ?','¿Está listo el site para los rastreadores de motores de búsqueda?',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(299,2,'Site working copy','Copia de trabajo del site',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(300,2,'Is this site the current working copy ?','¿Es este site la copia de trabajo actual?',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(301,2,'Ga key','Clave Ga',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(302,2,'The google analytics key without javascript stuff ( e.g. UA-9876614-1 )','La clave de Google Analytics sin javascript (por ejemplo, UA-9876614-1)',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(303,2,'Gwt key','Clave Gwt',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(304,2,'The google webmaster tools filename without google and .html ( e.g. a913b555ba9b4f13 )','El nombre de archivo de las herramientas para webmasters de Google sin Google y. Html (por ejemplo, a913b555ba9b4f13)',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(305,2,'Xiti code','Código Xiti',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(306,2,'The xiti html code','El código html xiti',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(307,2,'Ga email','Correo electrónico Ga',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(308,2,'Required to display google analytics data into Diem','Necesario para mostrar los datos en Google Analytics de Diem',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(309,2,'Ga password','Contraseña Ga',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(310,2,'Diem mail support is <strong>-NOT-</strong> completed. Please use the symfony 1.4 mail service instead','El soporte para correo electrónico de DIEM <strong>no está terminado</strong>. Por favor use el servicio de correo de Symfony 1.4',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(311,2,'Duplicate','Duplicado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(312,2,'Source','Fuente',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(313,2,'Target','Meta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(314,2,'Dm catalogue','Catálogo de Dm',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(315,2,'Meta','Meta',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(316,2,'Menu','Menú',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(317,2,'Internal search engine','Motor de búsqueda interno',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(318,2,'Reload index','Actualizar índice',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(319,2,'Configure Google Analytics','Configurar Google Analytics',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(320,2,'Configure Google Webmaster Tools','Configurar Google Webmaster Tools',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(321,2,'Position','Posición',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(322,2,'Urls','URL',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(323,2,'Old url','Url antigua',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(324,2,'New url','Nueva url',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(325,2,'Group','Grupo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(326,2,'Value','Valor',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(327,2,'Credentials','Credenciales',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(328,2,'Class','Clase',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(329,2,'Module','Módulo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(330,2,'Action','Acción',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(331,2,'Uri','URI',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(332,2,'Env','Desarrollo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(333,2,'Super admin','Super Administrador',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(334,2,'The admin code editor is <strong>-NOT-</strong> completed yet an may not work','El editor de código de administración <strong>no</strong> está completado todavía y puede no funcionar',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(335,2,'List','Lista',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(336,2,'Form','Formulario',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(337,2,'Administration','Administración',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(338,2,'text','texto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(339,2,'Index state','Índice de estado',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(340,2,'Index maintenance','Mantenimiento del índice',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(341,2,'Search completed in %1% ms','Búsqueda efectuada en %1% ms',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(342,2,'No results','No hay resultados',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(343,2,'Edit template code','Modificar el código de plantilla',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(344,2,'Edit component code','Código de componente Edit',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(345,2,'Drag & Drop a media here','Arastre y suelte un medio aquí',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(346,2,'Add a link to the text title','Añadir un vínculo al título del texto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(347,2,'Add a link to the text media','Añadir un enlace al medio del texto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(348,2,'Drag & Drop a page or enter an url','Arrastrar y suelte una página o introducir una dirección url',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(349,2,'Drag & drop a page here from the PAGES panel, or write an url','Arrastre y suelte una página aquí desde el panel de páginas, o escribir una dirección url',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(350,2,'Title position','Posición del título',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(351,2,'Outside','Exterior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(352,2,'Inside','Interior',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(353,2,'JPG quality','Calidad JPG',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(354,2,'Dimensions','Dimensiones',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(355,2,'Leave empty to use default quality','Dejar vacío para usar calidad por defecto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(356,2,'Separator','Separador',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(357,2,'Include current page','Incluir la página actual',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(358,2,'This CSS class is applied to the body tag','Esta clase CSS se aplica a la etiqueta body',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(359,2,'Revision history of %1%','Historia de revisiones de %1%',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(360,2,'Revision history','Historia de revisiones',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(361,2,'Field','Campo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(362,2,'Difference','Diferencia',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(363,2,'No revision for %1% with culture \"%2%\"','No hay revisión para %1% con la cultura \"%2%\"',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(364,2,'Body','Cuerpo',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(365,2,'Others','Otros',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(366,2,'Description','Descripción',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(367,2,'Vars','Vars',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(368,2,'From email','De correo electrónico',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(369,2,'To email','Para correo electrónico',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(370,2,'Updating project','Actualizando el proyecto',NULL,'2014-09-08 00:32:09','2014-09-08 00:32:09'),(371,2,'Cache clearing','Vaciar la caché',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(372,2,'Page synchronization','Sincronización de páginas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(373,2,'SEO synchronization','Sincronización de SEO',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(374,2,'Interface regeneration','Regeneración de la interfaz',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(375,2,'Something went wrong when updating project','Algo salió mal al actualizar el proyecto',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(376,2,'Send reports','Enviar informes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(377,2,'Receive reports','Recibir los informes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(378,2,'Open Google Analytics page','Abrir página de Google Analytics',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(379,2,'Open Google Webmaster Tools page','Abrir página de Herramientas para webmasters de Google',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(380,2,'Revert to revision %1%','Revertir a la revisión %1%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(381,2,'%1% has been reverted to version %2%',' %1% se ha revuelto a la versión %2%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(382,2,'To enable it, provide a google analytics access','Para habilitarlo debe tener acceso a Google Analytics',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(383,2,'This chart is currently not available','Este gráfico no está disponible',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(384,2,'Generate %1% random %2%','Generar %1% aleatorios %2%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(385,2,'Options','Opciones',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(386,2,'Drag & Drop an image here','Arrastre y suelte una imagen aquí',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(387,2,'\"%value%\" is not a valid link.','\"%value%\" no es un enlace válido.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(388,2,'\"%value%\" is not a valid directory name.','\"%value%\" no es un directorio válido.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(389,2,'History','Historial',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(390,2,'Set up a cron to update the search index','Configurar cron para actualizar los índices de búsqueda',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(391,2,'Set up a cron to update the sitemap','Configurar cron para actualizar el mapa del sitio',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(392,2,'Most UNIX and GNU/Linux systems allows for task planning through a mechanism known as cron. The cron checks a configuration file (a crontab) for commands to run at a certain time.','La mayoría de sistemas UNIX y GNU/Linux permiten planificar tareas mediante un mecanismo llamado cron. Cron examina un fichero de configuración (una crontab) en busca de comandos a ejecutar cada cierto tiempo.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(393,2,'For more information on the crontab configuration file format, type man 5 crontab in a terminal.','Para más información sobre la configuración de crontab, escriba \"man 5 contab\" en una terminal',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(394,2,'Open %1% and add the line:','Abrir %1% y adicionar la línea',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(395,2,'Cut','Cortar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(396,2,'Copy','Copiar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(397,2,'Paste','Pegar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(398,2,'Drag & drop links here from the left PAGE panel','Arrastre y suelte los enlaces desde el panel izquierdo \"Página\"',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(399,2,'Click to edit, drag to sort','Clic para editar, arrastrar para ordenar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(400,2,'Code generation','Generación de código',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(401,2,'%user% is browsing this page, you should not modify it now.','%user% está navegando en está página, no debería modificarla en este instante.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(402,2,'or create an external link','o cree un enlance externo',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(403,2,'Back to the parent folder','Atrás (carpeta padre)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(404,2,'Website','Sitio Web',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(405,2,'Message','Mensaje',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(406,2,'Signin','Iniciar sesión',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(407,2,'Signout','Cerrar sesión',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(408,2,'Back to admin','Atrás (administración)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(409,2,'Back to site','Atrás (sitio)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(410,2,'Invalid mime type (%mime_type%).','',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(411,2,'Download','Descargar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(412,2,'Move to','Mover a',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(413,2,'[1] And one more...|(1,+Inf] And %1% more...','[1] Y uno más ...|[1,+Inf] y %1% más...',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(414,2,'Go to admin','Ir a la administración',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(415,2,'Search for a widget','Buscar componente (widget)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(416,2,'Sort by %field%','Ordenar por %field%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(417,2,'contextual','Contextual',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(418,2,'Accepts pages and urls','Aceptar páginas y URLs',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(419,2,'Accepts medias and urls','Aceptar medias y URLs',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(420,2,'Accepts pages, medias and urls','Aceptar paginas, medias y URLs',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(421,2,'Drag pages around to move and sort them.','Arrastre las páginas para ordenarlas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(422,2,'Manage metas','Administrar metas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(423,2,'Reorder pages','Reordenar páginas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(424,2,'Manage pages','Administrar páginas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(425,2,'Click to edit','Clic para editar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(426,2,'Click any value in the table to modify it.','Clic en cualquier valor de la tabla para modificarlo',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(427,2,'Choose columns to display in the table.','Selecciona una columna para mostrar en la tabla',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(428,2,'Groups & Permissions','Grupos y Permisos',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(429,2,'File %file% does not exist','El fichero %file% no existe',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(430,2,'File %file% can not be deleted because a record needs it','El fichero %file% no puede ser eliminado porque es usado por un registro',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(431,2,'Add widgets','Adicionar contenidos (Widgets)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(432,2,'Back to %1%','Atrás hasta %1%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(433,2,'Revision %number%','Revisión %number%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(434,2,'My account','Mi cuenta',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(435,2,'The two passwords must be the same.','Las dos contraseñas deben coincidir',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(436,2,'Connected','Conectado',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(437,2,'If you set a href, a link will be inserted into the title','Si escribe un HREF se inserta un vínculo dentro del título',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(438,2,'Template','Plantillas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(439,2,'Areas','Áreas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(440,2,'Nb pages','Páginas Nb',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(441,2,'Manage XML sitemap','Administrar Mapa del Sitio (XML)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(442,2,'Pages','Páginas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(443,2,'Automatic SEO','SEO Automático',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(444,2,'Not connected','No conectado',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(445,2,'Keywords','Palabras clave',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(446,2,'Secure','Seguro',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(447,2,'Indexable','Indexable',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(448,2,'Select columns','Seleccionar columnas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(449,2,'Successfully loremized','Aleatorizado satisfactoriamente',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(450,2,'This file is writable','Este fichero se puede escribir',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(451,2,'Loading','Cargando',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(452,2,'Image','Imagen',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(453,2,'Results','Resultados',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(454,2,'Desc','Desc',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(455,2,'Publication','Publicación',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(456,2,'Layout','Decorador',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(457,2,'Parent','Padre',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(458,2,'Slug','Slug',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(459,2,'Controllers','Controlador',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(460,2,'Templates','Plantilla',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(461,2,'Stylesheets','Estilos',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(462,2,'Presentation','Presentación',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(463,2,'Send','Enviar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(464,2,'No module to manage.','No hay módulos para administrar',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(465,2,'Variables you can use here:','Variables que pueden ser usadas aquí',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(466,2,'1. Edit meta generation rules','1. Editar reglas de generación de metas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(467,2,'Preview modifications without applying changes to the site','Previsualizar modificaciones sin aplicar los cambios en el sitio',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(468,2,'Save modifications and apply changes to the site','Salvar modificaciones y aplicar los cambios en el sitio',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(469,2,'2. Preview generated metas','Previsualizar METAS generados',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(470,2,'The page url, without domain name. Must be unique. If the slug does not start with a \'/\', the parent slug is added.','La dirección de la página (URL) sin el nombre de dominio. Debe ser única. Si el slug no comienza con \'/\', se añade el slug del padre.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(471,2,'The page title, without prefix nor suffix. Should be unique.','El título de la página, sin prefijos o sufijos. Debe ser único.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(472,2,'The page name, used by links to this page. Should be unique.','El nombre de la pagina, usado en los enlaces a la misma. Debe ser único.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(473,2,'Assign first header here or let it blank to let the designer choose it. Should be unique.','Asigne la cabecera principal aquí o déjelo en blanco para dejar al diseñador escoger. Debe ser único.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(474,2,'The page description meta, frequently displayed in search engines result page.','Las etiquetas meta de descripción de la página, mostradas frecuentemente en los resultados de búsqueda.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(475,2,'Provides additional meta informations to the page. Also used by Diem internal search engine.','Proporciona información META adicional para la página. Usado también por Diem para el buscador interno.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(476,2,'Requests per minute','Peticiones por minuto',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(477,2,'Errors per minute','Errores por minuto',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(478,2,'Latency in ms','Latencia en milisegundos(ms)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(479,2,'Memory used in %','Memoria usada en porcetaje(%)',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(480,2,'Pages per month','Páginas por mes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(481,2,'Visitors per month','Visitantes por mes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(482,2,'Visitors','Visitantes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(483,2,'Bounce rate','Calificación',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(484,2,'No results for \"%1%\"','No hay resultados para \"%1%\"',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(485,2,'Results %1% to %2% of %3%','Resultados %1% a %2% de %3%',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(486,2,'Items','Elementos',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(487,2,'Save this %1% to access to the gallery','Guarde este %1% para acceder a la galería',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(488,2,'UL CSS class','Clase CSS UL',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(489,2,'LI CSS class','Clase CSS LI',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(490,2,'Ascendant','Ascendente',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(491,2,'Descendant','Descendente',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(492,2,'Version','Versión',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(493,2,'The widget can not be rendered because its type does not exist anymore.','Este componente (widget) no puede ser representado porque no existe',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(494,2,'Link to this page:','Enlace a esta página',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(495,2,'Sentence','Frase',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(496,2,'The item was created successfully.','El elemento fue creado correctamente.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(497,2,'The item was created successfully. You can add another one below.','El elemento fue creado correctamente. Puede agregar otro abajo.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(498,2,'Heading 2','Encabezado 2',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(499,2,'Heading 3','Encabezado 3',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(500,2,'Heading 4','Encabezado 4',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(501,2,'Heading 5','Encabezado 5',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(502,2,'Bold','Negrita',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(503,2,'Italic','Cursiva',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(504,2,'Bulleted List','Lista de puntos',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(505,2,'Numeric List','Lista numerada',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(506,2,'Enlarge the editor','Ampliar el editor',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(507,2,'Permission','Permiso',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(508,2,'Nb sent mails','Núm. de correos enviados',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(509,2,'Advanced search','Búsqueda avanzada',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(510,2,'Today','Hoy',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(511,2,'Past %number% days','Últimos %number% días ',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(512,2,'This month','Este mes',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(513,2,'This year','Este año',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(514,2,'Pages per week','Páginas por semana',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(515,2,'Visitors per week','Visitantes a la semana',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(516,2,'Show extended options','Mostrar opciones extendidas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(517,2,'Hide extended options','Ocultar opciones extendidas',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(518,2,'Records','Récords',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(519,2,'Record','Récord',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10'),(520,2,'Enable Demographic tracking in GA code','Habilita el tracking demografico en GA.',NULL,'2014-09-08 00:32:10','2014-09-08 00:32:10');
/*!40000 ALTER TABLE `dm_trans_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_user`
--

DROP TABLE IF EXISTS `dm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `algorithm` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sha1',
  `salt` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_super_admin` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `forgot_password_code` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `forgot_password_code` (`forgot_password_code`),
  KEY `is_active_idx_idx` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_user`
--

LOCK TABLES `dm_user` WRITE;
/*!40000 ALTER TABLE `dm_user` DISABLE KEYS */;
INSERT INTO `dm_user` VALUES (1,'admin','admin@triaris.com','sha1','b6f1620f682b155a7a8711ff272d5689','8b97232db81a600ac3f4aed7371e6bf47908673a',1,1,'2014-09-20 17:12:38',NULL,'2014-09-08 00:32:08','2014-09-08 00:32:08');
/*!40000 ALTER TABLE `dm_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_user_group`
--

DROP TABLE IF EXISTS `dm_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_user_group` (
  `dm_user_id` bigint(20) NOT NULL DEFAULT '0',
  `dm_group_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dm_user_id`,`dm_group_id`),
  KEY `dm_user_group_dm_group_id_dm_group_id` (`dm_group_id`),
  CONSTRAINT `dm_user_group_dm_group_id_dm_group_id` FOREIGN KEY (`dm_group_id`) REFERENCES `dm_group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_user_group_dm_user_id_dm_user_id` FOREIGN KEY (`dm_user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_user_group`
--

LOCK TABLES `dm_user_group` WRITE;
/*!40000 ALTER TABLE `dm_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_user_permission`
--

DROP TABLE IF EXISTS `dm_user_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_user_permission` (
  `dm_user_id` bigint(20) NOT NULL DEFAULT '0',
  `dm_permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dm_user_id`,`dm_permission_id`),
  KEY `dm_user_permission_dm_permission_id_dm_permission_id` (`dm_permission_id`),
  CONSTRAINT `dm_user_permission_dm_permission_id_dm_permission_id` FOREIGN KEY (`dm_permission_id`) REFERENCES `dm_permission` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dm_user_permission_dm_user_id_dm_user_id` FOREIGN KEY (`dm_user_id`) REFERENCES `dm_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_user_permission`
--

LOCK TABLES `dm_user_permission` WRITE;
/*!40000 ALTER TABLE `dm_user_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `dm_user_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_widget`
--

DROP TABLE IF EXISTS `dm_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_widget` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_zone_id` bigint(20) NOT NULL,
  `module` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` bigint(20) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_zone_id_idx` (`dm_zone_id`),
  CONSTRAINT `dm_widget_dm_zone_id_dm_zone_id` FOREIGN KEY (`dm_zone_id`) REFERENCES `dm_zone` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_widget`
--

LOCK TABLES `dm_widget` WRITE;
/*!40000 ALTER TABLE `dm_widget` DISABLE KEYS */;
INSERT INTO `dm_widget` VALUES (1,1,'dmWidgetContent','title',NULL,-1,'2014-09-08 00:32:08'),(2,2,'dmUser','signin',NULL,-2,'2014-09-08 00:32:08'),(3,6,'pages','homepage','',2,'2014-09-10 04:47:30'),(5,7,'dmWidgetContent','text','',2,'2014-09-20 17:30:08'),(6,10,'dmWidgetContent','text','',1,'2014-09-20 17:32:06'),(7,11,'dmWidgetContent','text','',1,'2014-09-20 17:37:55'),(8,12,'dmWidgetContent','text','credits',1,'2014-09-20 19:49:47');
/*!40000 ALTER TABLE `dm_widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_widget_translation`
--

DROP TABLE IF EXISTS `dm_widget_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_widget_translation` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci,
  `lang` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`lang`),
  CONSTRAINT `dm_widget_translation_id_dm_widget_id` FOREIGN KEY (`id`) REFERENCES `dm_widget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_widget_translation`
--

LOCK TABLES `dm_widget_translation` WRITE;
/*!40000 ALTER TABLE `dm_widget_translation` DISABLE KEYS */;
INSERT INTO `dm_widget_translation` VALUES (1,'{\"text\":\"Page not found\",\"tag\":\"h1\"}','es'),(2,'[]','es'),(3,'[]','es'),(5,'{\"title\":\"\",\"text\":\"### Cont\\u00e1ctanos\\r\\n\\r\\nEstamos ubicados en Guayaquil - Ecuador, y puedes contactarnos a trav\\u00e9s de las siguentes v\\u00edas.\\r\\n<div class=\\\"menu_footer\\\">\\r\\n      <ul  class=\\\"elements\\\">\\r\\n        <li class=\\\"element\\\">\\r\\n<a class=\\\"link\\\" href=\\\"mailto:info@triaris.com\\\"><span class=\\\"icons icon-envelope\\\"><\\/span>info@triaris.com<\\/a>\\r\\n<\\/li>\\r\\n        <li class=\\\"element\\\"><span class=\\\"icons icon-user\\\"><\\/span>099 2829653<\\/li>\\r\\n      <\\/ul>\\r\\n<\\/div>\\r\\n\",\"mediaId\":null,\"titleLink\":\"\",\"mediaLink\":\"\",\"titlePosition\":\"outside\",\"width\":\"\",\"height\":\"\",\"legend\":\"\",\"method\":\"center\",\"background\":\"FFFFFF\",\"quality\":null}','es'),(6,'{\"title\":\"\",\"text\":\"### Somos Sociales\\r\\n\\r\\n\\r\\n<div class=\\\"menu_footer\\\">\\r\\n<ul>\\r\\n<li><a class=\\\"link\\\" target=\\\"_blank\\\" href=\\\"https:\\/\\/es-es.facebook.com\\/TriarisDesign\\\"><span class=\\\"icons icon-facebook social\\\"><\\/span><strong>\\/<\\/strong>TriarisDesign<\\/a><\\/li>\\r\\n<li><a class=\\\"link\\\" target=\\\"_blank\\\" href=\\\"https:\\/\\/twitter.com\\/TriarisDesign\\\"><span class=\\\"icons icon-twitter social\\\"><\\/span><strong>@<\\/strong>TriarisDesign<\\/a><\\/li>\\r\\n<li>\\r\\n<a class=\\\"link\\\" target=\\\"_blank\\\" href=\\\"http:\\/\\/www.linkedin.com\\/company\\/triaris-design-studio\\\"><span class=\\\"icons icon-linkedin social\\\"><\\/span><strong>company\\/<\\/strong>TriarisDesign<\\/a>\\r\\n<\\/li>\\r\\n<\\/ul>\\r\\n<\\/div>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\",\"mediaId\":null,\"titleLink\":\"\",\"mediaLink\":\"\",\"titlePosition\":\"outside\",\"width\":\"\",\"height\":\"\",\"legend\":\"\",\"method\":\"center\",\"background\":\"FFFFFF\",\"quality\":null}','es'),(7,'{\"title\":\"\",\"text\":\"<h2 class=\\\"\\\">Contrata<\\/h2>\\r\\n<h2 class=\\\"\\\">Publicidad<\\/h2>\\r\\n<h2 class=\\\"red\\\">Efectiva...<\\/h2>\\r\\n<a class=\\\"link cotiza\\\"href=\\\"https:\\/\\/es-es.facebook.com\\/TriarisDesign\\\">Click Aqu\\u00ed y Cot\\u00edzanos<\\/a>\\r\\n\\r\\n\",\"mediaId\":null,\"titleLink\":\"\",\"mediaLink\":\"\",\"titlePosition\":\"outside\",\"width\":\"\",\"height\":\"\",\"legend\":\"\",\"method\":\"center\",\"background\":\"FFFFFF\",\"quality\":null}','es'),(8,'{\"title\":\"\",\"text\":\"**\\u00a9 2015 By Triaris Design Studio  |  ALL RIGHTS RESERVED.**\\r\\n\",\"mediaId\":null,\"titleLink\":\"\",\"mediaLink\":\"\",\"titlePosition\":\"outside\",\"width\":\"\",\"height\":\"\",\"legend\":\"\",\"method\":\"center\",\"background\":\"FFFFFF\",\"quality\":null}','es');
/*!40000 ALTER TABLE `dm_widget_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dm_zone`
--

DROP TABLE IF EXISTS `dm_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dm_zone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dm_area_id` bigint(20) NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dm_area_id_idx` (`dm_area_id`),
  CONSTRAINT `dm_zone_dm_area_id_dm_area_id` FOREIGN KEY (`dm_area_id`) REFERENCES `dm_area` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dm_zone`
--

LOCK TABLES `dm_zone` WRITE;
/*!40000 ALTER TABLE `dm_zone` DISABLE KEYS */;
INSERT INTO `dm_zone` VALUES (1,1,NULL,NULL,-1),(2,2,NULL,NULL,-2),(3,3,NULL,NULL,-3),(4,4,NULL,NULL,-4),(5,5,NULL,NULL,-5),(6,6,NULL,NULL,-6),(7,7,'span7','',1),(8,8,NULL,NULL,-8),(9,9,NULL,NULL,-9),(10,7,'offset1 span7','',2),(11,7,'span8 omega','',3),(12,7,NULL,NULL,4);
/*!40000 ALTER TABLE `dm_zone` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-20 19:25:56
